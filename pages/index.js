import {useState} from "react";

export default function Home() {
  const [textToSearch, setTextToSearch] = useState("");
  const [searchResults, setSearchResults] = useState(null)

  const makeApiCall = () => {
    // implement API CALL path: /api/search
    // query parameters q: string
  };

  return (
    <div className="App">
      <header className="App-header">
        <h1>🇫🇷 French Dictionary!</h1>
      </header>

      <section className="App-input-search">
        <input
          type="text"
          value={textToSearch}
          onChange={(e) => setTextToSearch(e.target.value)}
        />
        <button onClick={makeApiCall}>Search</button>
      </section>

      <section>
        <h2>Results</h2>
        <ul>
          {
            searchResults?.definitions.results?.map(result => {
              return result?.lexicalEntries?.map(lexicalEntry => {
                return lexicalEntry.entries.map(entry => {
                  return entry.senses.map(sense => {
                    return (
                      <div>
                        <div>
                          <strong>Definition:</strong>
                          {sense.definitions}
                        </div>
                        <div>
                          Examples:
                          <ul>
                            {sense.examples?.map(example => {
                              return (
                                <li>{example.text}</li>
                              )
                            })}
                          </ul>
                        </div>
                      </div>
                    )
                  })
                })
              })
            })
          }
        </ul>
      </section>
    </div>
  );
}