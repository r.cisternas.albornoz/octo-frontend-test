import axios from 'axios';

const OXFORD_APP_ID = 'b9330d65';
const OXFORD_APP_KEY = '1378d509b347ccc20c71689c562debb9';

const getApiUrl = (text) => {
  const encodedText = encodeURI(text);
  return `https://od-api.oxforddictionaries.com/api/v2/entries/fr/${encodedText}?fields=definitions%2Cdomains%2Cetymologies%2Cexamples%2Cpronunciations%2Cregions%2Cregisters%2CvariantForms&strictMatch=false`;
}

const getEntries = async (text) => {
  const API_URL = getApiUrl(text);
  const res = await axios.get(API_URL, {
    headers: {
      'app_id': OXFORD_APP_ID,
      'app_key': OXFORD_APP_KEY,
    },
  });
  return res?.data;
}

export default async (req, res) => {
  const {query} = req;
  const {q: text} = query;
  let definitions;
  console.log(query)
  try {
    definitions = await getEntries(text);
    // definitions = {}
  } catch (error) {
    console.log('error when getting translation', error);
  }
  res.statusCode = 200
  res.json({definitions})
}